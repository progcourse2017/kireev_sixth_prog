#include <iostream>
#include <ctime>

using namespace std;

unsigned long int cycle(int _numberAlement);
unsigned long int recursion(int _numberAlement);

int main(unsigned argc, char **argv) {
	int numberAlement;
	unsigned long int result;
	long double startTime, endTime;

	cout << "Enter number of alement: ";
	cin >> numberAlement;

	cout << "Cycle\n";
	startTime = clock();
	result = cycle(numberAlement);
	endTime = clock();
	cout << "Result: " << result << endl;
	cout << "Time: " << endTime - startTime << "ms" << endl;

	cout << "\n";

	cout << "Recursion\n";
	startTime = clock();
	result = recursion(numberAlement);
	endTime = clock();
	cout << "Result: " << result << endl;
	cout << "Time: " << endTime - startTime << "ms" << endl;

	system("pause");
	return 0;
}

unsigned long int cycle(int _numberAlement) {
	unsigned long int *array = new unsigned long int[_numberAlement];
	array[0] = 1;
	array[1] = 1;

	if (_numberAlement == 1 || _numberAlement == 2)
		return 1;

	for (int count = 2; count < _numberAlement; count++)
		array[count] = array[count - 2] + array[count - 1];

	return array[_numberAlement - 1];
}

unsigned long int recursion(int _numberAlement)
{
	if (_numberAlement == 1 || _numberAlement == 2)
		return 1;
	return recursion(_numberAlement - 2) + recursion(_numberAlement - 1);
}